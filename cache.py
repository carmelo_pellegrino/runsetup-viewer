import json
import socket

class Cache(dict):

  def __init__(self, f = None, s = None, m = None):
    if f:
      self.update(json.load(f))
    if s:
      with open(s) as fil:
        self.update(json.load(fil))
    if m:
      self.update(m)


  def __getitem__(self, key):
    if key in self:
      return super(Cache, self).__getitem__(key)
    else:
      try:
        resp = socket.gethostbyaddr(key)
        if len(resp[1]):
          name = resp[1][0]
        else:
          name = resp[0]
        self[key] = name
        return name
      except:
        self[key] = key
        return key

