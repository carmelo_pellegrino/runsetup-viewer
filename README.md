# Description
This simple script allows a fast visualisation in tabular of several informations contained into a TriDAS runsetup. It also provides a very basic visualisation of the footprint of the detector defined in the geometry section.
In order to help the reader to understand the TriDAS layout defined in the runsetup, the IP addresses are replaced by the corresponding hostname (where available), by means of calls to the DNS. The user is allowed to specify its own conversion table via a configuration file.
It also allows to create an HTML version of the tables and footprint (in SVG).

# Dependencies
* Python version 2.7 (a port to 3.x will be provided soon)
* svgwrite (pip install svgwrite)
* prettytable (pip install prettytable)
* argparse (pip install pyargs)

# How to use

```
#!shell
./runsetup-table.py [-h] [--hosts ip2host.json] [--html] runsetup.json
```

Use runsetup-table.py -h to view the help.
