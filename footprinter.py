import svgwrite

def terminal_size():
  import fcntl, termios, struct, sys
  try:
    h, w, hp, wp = struct.unpack('HHHH',
      fcntl.ioctl(sys.stdout.fileno(), termios.TIOCGWINSZ,
       struct.pack('HHHH', 0, 0, 0, 0)))
    return w, h
  except:
    return (84, 24)

def genindex(label, n):
  i = 0
  while i < n:
    yield label + str(i)
    i += 1

def coords(point, size, max_x, max_y, min_x, min_y):
  return ((point[0] - min_x) * size[0] / (max_x - min_x), (point[1] - min_y) * size[1] / (max_y - min_y))

class Footprint(list):
  def __init__(self, datacard, npmt = 6, ntowers = 8):
    for t in genindex('TOWER_', ntowers):
      x_pos = 0
      y_pos = 0
      floor1 = datacard[t]['FLOOR_1']

      for p in genindex('PMT_', npmt):
        x = float(floor1[p]['X'])
        y = float(floor1[p]['Y'])
        x_pos += x
        y_pos += y

      x_pos = x_pos / npmt
      y_pos = y_pos / npmt
      self += [(x_pos, y_pos)]

  def __str__(self, marker = '+'):
    dimensions = terminal_size()

    max_x = max([x[0] for x in self])
    min_x = min([x[0] for x in self])

    max_y = max([x[1] for x in self])
    min_y = min([x[1] for x in self])

    delta_x = (max_x - min_x) * 0.1

    max_x = max_x + delta_x
    min_x = min_x - delta_x

    delta_y = (max_y - min_y) * 0.1

    max_y = max_y + delta_y
    min_y = min_y - delta_y

    term_coords = []
    for point in self:
      temp = coords(point, dimensions, max_x, max_y, min_x, min_y)
      term_coords += [(int(temp[0]), int(temp[1]))]

    foot_map = {}

    for i in term_coords:
      if i[1] in foot_map:
        foot_map[i[1]] += (i[0],)
      else:
        foot_map[i[1]] = [i[0]]

    idxs = foot_map.keys()
    idxs.sort()

    cursor_x = 0
    cursor_y = 0

    r = ''

    for i in idxs:
      r += '\n' * (i - cursor_y)
      cursor_y = i
      cursor_x = 0
      ys = foot_map[i]
      ys.sort()
      for j in ys:
        r += ' ' * (j - cursor_x) + marker
        cursor_x = j + 1

    return r

  def to_svg(self, dimensions = (400, 400)):
    dwg = svgwrite.Drawing()
    dwg.add(dwg.rect(insert = (0, 0), size = dimensions, fill = 'white'))

    max_x = max([x[0] for x in self])
    min_x = min([x[0] for x in self])

    max_y = max([x[1] for x in self])
    min_y = min([x[1] for x in self])

    delta_x = (max_x - min_x) * 0.1

    max_x = max_x + delta_x
    min_x = min_x - delta_x

    delta_y = (max_y - min_y) * 0.1

    max_y = max_y + delta_y
    min_y = min_y - delta_y

    for point in self:
      dwg.add(dwg.circle(r = 3, center = coords(point, dimensions, max_x, max_y, min_x, min_y), fill="red"))

    return dwg.tostring()

