from prettytable import PrettyTable

def get_hm_table(datacard, cache):
  return get_x_table(datacard, cache, 'HM')

def get_tcpu_table(datacard, cache):
  return get_x_table(datacard, cache, 'TCPU')

def get_general_x_table(datacard, cache, role):
  t = PrettyTable()

  x = datacard[role]

  t.add_column("item", [], align = 'l')
  t.add_column("value", [], align = 'r')

  for elem in x:
    if type(x[elem]) in (str, unicode):
      t.add_row([elem, x[elem]])

  return t

def get_x_table(datacard, cache, role):
  t = PrettyTable()

  total = 0

  t.add_column('host', [], align = 'l')
  t.add_column('# instances', [], align = 'r')

  for i in datacard[role]['HOSTS']:
    total += int(i['N_INSTANCES'])
    t.add_row([cache[i['CTRL_HOST']], i['N_INSTANCES']])

  return (role + 's (' + str(total) + ')', t)

def get_tsv_table(datacard, cache):
  t = PrettyTable()

  t.add_column('host', [cache[datacard['TSV']['CTRL_HOST']]], align = 'l')
  t.add_column('log level', [datacard['TSV']['LOG_LEVEL']], align = 'r')

  return ("TSV", t)

def get_tsc_table(datacard, cache):
  t = get_general_x_table(datacard, cache, 'TSC')

  return ("TSC", t)

def get_em_table(datacard, cache):
  t = get_general_x_table(datacard, cache, 'EM')

  t.add_row(["host", cache[datacard['EM']['CTRL_HOST']]])

  return ("EM", t)

def get_fcm_table(datacard, cache):
  fcms = datacard['FCM_ENDPOINTS']
  t = PrettyTable()
  t.add_column('host', [], align = 'l')
  t.add_column('# instances', [], align = 'r')
  t.add_column('ports', [], align = 'l')

  m = {}

  for fcm in fcms:
    host = cache[fcm['DATA_HOST']]
    if host in m:
      m[host] += [fcm['DATA_PORT']]
    else:
      m[host] = [fcm['DATA_PORT']]

  for elem in m:
    t.add_row([elem, len(m[elem]), ' '.join(m[elem])])

  return ('FCMs (' + str(len(fcms)) + ')', t)
