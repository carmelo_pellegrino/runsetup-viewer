#!/usr/bin/env python2
#-*- encode: UTF-8 -*-

import argparse
import json

from cache import Cache
from tabulazer import get_hm_table
from tabulazer import get_tcpu_table
from tabulazer import get_tsv_table
from tabulazer import get_tsc_table
from tabulazer import get_fcm_table
from tabulazer import get_em_table
from footprinter import Footprint

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Options:')
    parser.add_argument(
        'file_json',
        metavar='runsetup.json',
        type=str,
        nargs=1,
        help="runsetup file to analyse.")
    parser.add_argument(
        '--hosts',
        dest='cache_json',
        metavar='ip2host.json',
        default=None,
        type=str,
        nargs=1,
        help="IP address to hostname file.")
    parser.add_argument(
        '--html',
        dest='html',
        default=False,
        action='store_true',
        help="give HTML output.")

    args = parser.parse_args()

    with open(args.file_json[0]) as f:
        datacard = json.load(f)

    cache = Cache(s=args.cache_json)

    tables = []

    tables += [get_hm_table(datacard, cache)]
    tables += [get_tcpu_table(datacard, cache)]
    tables += [get_tsv_table(datacard, cache)]
    tables += [get_tsc_table(datacard, cache)]
    tables += [get_em_table(datacard, cache)]
    tables += [get_fcm_table(datacard, cache)]

    footprint = Footprint(datacard)

    if args.html:
        for i in tables:
            print '<h3>' + i[0] + '</h3>'
            print i[1].get_html_string()
        print '<h3>Footprint</h3>'
        print footprint.to_svg()
    else:
        for i in tables:
            print i[0]
            print i[1]
            print
        print 'Footprint:\n', footprint
